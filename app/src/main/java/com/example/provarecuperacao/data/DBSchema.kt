package com.example.provarecuperacao.data

object DBSchema {
    object CarTable{
        const val TABLENAME = "cars"
        const val ID = "c_id"
        const val CATEGORY = "c_category"
        const val MIN_VALUE = "c_min_value"
        const val ENTRY_DATE = "c_entry_date"
        const val DESCRIPTION = "c_description"

        fun getCreateTableQuery(): String{
            return """ 
                CREATE TABLE IF NOT EXISTS $TABLENAME (
                    $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    $CATEGORY TEXT NOT NULL,
                    $MIN_VALUE REAL NOT NULL,
                    $ENTRY_DATE TEXT NOT NULL,
                    $DESCRIPTION TEXT NOT NULL
                );
            """.trimIndent()
        }
    }
}