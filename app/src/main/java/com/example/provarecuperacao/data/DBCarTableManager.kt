package com.example.provarecuperacao.data

import android.content.ContentValues
import android.content.Context
import com.example.provarecuperacao.model.Car

class DBCarTableManager(context : Context) {
    companion object{
        val COLS = arrayOf(DBSchema.CarTable.ID,
                            DBSchema.CarTable.CATEGORY,
                            DBSchema.CarTable.MIN_VALUE,
                            DBSchema.CarTable.ENTRY_DATE,
                            DBSchema.CarTable.DESCRIPTION)
    }

    private val dbHelper = DBHelper(context)

    fun insert(car: Car) {
        val cv = ContentValues()
        cv.put(DBSchema.CarTable.CATEGORY, car.getCategory())
        cv.put(DBSchema.CarTable.MIN_VALUE, car.getMinValue())
        cv.put(DBSchema.CarTable.ENTRY_DATE, car.getEntryDate())
        cv.put(DBSchema.CarTable.DESCRIPTION, car.getDescription())
        val db = this.dbHelper.writableDatabase
        db.insert(DBSchema.CarTable.TABLENAME, null, cv)
        db.close()
    }

    fun getAllCars(): ArrayList<Car> {
        val cars : ArrayList<Car> = ArrayList()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(DBSchema.CarTable.TABLENAME, COLS, null, null, null, null, null)
        while(cur.moveToNext()){
            val id = cur.getLong(cur.getColumnIndexOrThrow(DBSchema.CarTable.ID))
            val category = cur.getString(cur.getColumnIndexOrThrow(DBSchema.CarTable.CATEGORY))
            val minValue = cur.getDouble(cur.getColumnIndexOrThrow(DBSchema.CarTable.MIN_VALUE))
            val entryDate = cur.getString(cur.getColumnIndexOrThrow(DBSchema.CarTable.ENTRY_DATE))
            val description = cur.getString(cur.getColumnIndexOrThrow(DBSchema.CarTable.DESCRIPTION))
            val car = Car(id,category,minValue,entryDate,description)
            cars.add(car)
        }
        db.close()
        return cars
    }
}