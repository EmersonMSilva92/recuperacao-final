package com.example.provarecuperacao.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context) : SQLiteOpenHelper(context, DBNAME, null, DBVERSION) {
    companion object{
        private const val DBNAME = "cartable.db"
        private const val DBVERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBSchema.CarTable.getCreateTableQuery())
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }
}