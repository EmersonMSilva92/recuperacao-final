package com.example.provarecuperacao.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.provarecuperacao.R
import com.example.provarecuperacao.data.DBCarTableManager
import com.example.provarecuperacao.model.Car
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class AddCarActivity : AppCompatActivity() {

    private lateinit var spinnerCat : Spinner
    private lateinit var etxtMinValue : EditText
    private lateinit var etxtEntryDate : EditText
    private lateinit var etxtDescription : EditText
    private lateinit var btnToday : Button
    private lateinit var btnAddCar : Button
    private lateinit var dbCars : DBCarTableManager

    private val arraySpinner = arrayOf("Sedan","SUV","Camionete","Hatch","Motocicleta")

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_car)
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,arraySpinner)

        this.spinnerCat = findViewById(R.id.spinnerCat)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        this.spinnerCat.adapter = arrayAdapter
        this.etxtMinValue = findViewById(R.id.etxtMinValue)
        this.etxtDescription = findViewById(R.id.etxtDescription)
        this.etxtEntryDate = findViewById(R.id.etxtEntryDate)
        this.btnToday = findViewById(R.id.btnToday)
        this.btnAddCar = findViewById(R.id.btnAddCar)
        this.dbCars = DBCarTableManager(this)
    }

    fun onClickToday(v: View){
        val current = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val formatted = current.format(formatter)

        this.etxtEntryDate.setText(formatted)

    }

    fun onClickAdd(v: View){
        val category = this.spinnerCat.selectedItem.toString()
        val minValue = this.etxtMinValue.text.toString()
        val entryDate = this.etxtEntryDate.text.toString()
        val description = this.etxtDescription.text.toString()

        if(minValue.isNotEmpty()){
            if(entryDate.isNotEmpty()){
                if(description.isNotEmpty()){
                    val car = Car(category, minValue.toDouble(),entryDate,description)
                    dbCars.insert(car)

                    Toast.makeText(this,R.string.txt_car_added_sucessfully,Toast.LENGTH_SHORT).show()
                    finish()
                }   else{
                    Toast.makeText(this,R.string.txt_error_description,Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,R.string.txt_error_entry_date,Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this,R.string.txt_error_min_value,Toast.LENGTH_SHORT).show()
        }



    }
}