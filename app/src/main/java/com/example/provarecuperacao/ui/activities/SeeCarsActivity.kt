package com.example.provarecuperacao.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.provarecuperacao.R
import com.example.provarecuperacao.data.DBCarTableManager
import com.example.provarecuperacao.ui.lists.CarListAdapter

class SeeCarsActivity : AppCompatActivity() {
    private lateinit var rvCarList : RecyclerView
    private lateinit var dbCars : DBCarTableManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_cars)

        this.rvCarList = findViewById(R.id.rvCarList)
        this.dbCars = DBCarTableManager(this)
        this.rvCarList.layoutManager = LinearLayoutManager(this)
        this.rvCarList.adapter = CarListAdapter(dbCars.getAllCars())
    }
}