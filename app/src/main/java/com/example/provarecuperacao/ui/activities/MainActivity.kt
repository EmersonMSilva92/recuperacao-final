package com.example.provarecuperacao.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.provarecuperacao.R

class MainActivity : AppCompatActivity() {

    private lateinit var btnAddCarActivity : Button
    private lateinit var btnSeeCarsActivity : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAddCarActivity = findViewById(R.id.btnAddCarActivity)
        btnSeeCarsActivity = findViewById(R.id.btnSeeCarsActivity)
    }

    fun onClickAddCarActivity(v: View){
        val intent = Intent(this, AddCarActivity::class.java)
        startActivity(intent)
    }

    fun onClickSeeCarsActivity(v: View){
        val intent = Intent(this, SeeCarsActivity::class.java)
        startActivity(intent)
    }
}