package com.example.provarecuperacao.ui.lists

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.provarecuperacao.R
import com.example.provarecuperacao.model.Car

class CarListAdapter(private val carList : ArrayList<Car>) : RecyclerView.Adapter<CarListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.view_card_car, parent, false)
        return CarListViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: CarListViewHolder, position: Int) {
        holder.bind(this.carList[position])
    }

    override fun getItemCount(): Int {
        return this.carList.size
    }
}