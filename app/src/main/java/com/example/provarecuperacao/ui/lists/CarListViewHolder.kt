package com.example.provarecuperacao.ui.lists

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.provarecuperacao.R
import com.example.provarecuperacao.model.Car

class CarListViewHolder (
    itemView: View,
    private val adapter : CarListAdapter
) : RecyclerView.ViewHolder(itemView) {

    private val txtCardCat: TextView = itemView.findViewById(R.id.txtCardCat)
    private val txtCardMinValue: TextView = itemView.findViewById(R.id.txtCardMinValue)
    private val txtCardEntryDate: TextView = itemView.findViewById(R.id.txtCardEntryDate)
    private val txtCardDescription: TextView = itemView.findViewById(R.id.txtCardDescription)
    private val linearLayoutDescription: LinearLayout = itemView.findViewById(R.id.linearLayoutDescription)
    private val downArrow: ImageView = itemView.findViewById(R.id.downArrow)
    private val upArrow: ImageView = itemView.findViewById(R.id.upArrow)
    private lateinit var car: Car

    init{
        this.linearLayoutDescription.setOnClickListener{
            if(txtCardDescription.visibility == View.GONE){
                downArrow.visibility = View.GONE
                upArrow.visibility = View.VISIBLE
                txtCardDescription.visibility = View.VISIBLE
            }   else{
                downArrow.visibility = View.VISIBLE
                upArrow.visibility = View.GONE
                txtCardDescription.visibility = View.GONE
            }
        }
    }

    fun bind(car: Car){
        this.car = car
        this.txtCardCat.text = car.getCategory()
        this.txtCardMinValue.text = car.getMinValue().toString()
        this.txtCardEntryDate.text = car.getEntryDate()
        this.txtCardDescription.text = car.getDescription()

    }
}