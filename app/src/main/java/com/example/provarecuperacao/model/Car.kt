package com.example.provarecuperacao.model

class Car(private val category: String, private val minValue: Double, private val entryDate: String, private val description: String) {
    private var id : Long = 0

    constructor(id : Long, category: String,minValue: Double,entryDate: String,description: String ): this(category, minValue, entryDate, description){
        this.id = id
    }

    fun getCategory(): String{
        return category
    }

    fun getMinValue(): Double{
        return minValue
    }

    fun getEntryDate(): String{
        return entryDate
    }

    fun getDescription(): String{
        return description
    }

    fun getId() : Long{
        return id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Car

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}